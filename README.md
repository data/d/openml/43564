# OpenML dataset: Parking-Statistics-in-North-America

https://www.openml.org/d/43564

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

ABOUT
This dataset identifies areas within a city where drivers are experiencing difficulty searching for parking. Cities can use this data to identify problem areas, adjust signage, and more. Only cities with a population of more than 100,000 are included. 
Data
Some variables to highlight:

AvgTimeToPark: The average time taken to search for parking (in minutes)
AvgTimeToParkRatio: The ratio between the average time taken to search for parking and of those not searching for parking in the current geohash
TotalSearching: The number of drivers searching for parking
PercentSearching: The percentage of drivers that were searching for parking
AvgUniqueGeohashes: The average number of unique geohashes at the 7 character level (including neighbouring and parking geohashes) that were driven in among vehicles that searched for parking
AvgTotalGeohashes: The average number of all geohashes at the 7 character level (including neighbouring and parking geohashes) that were driven in among vehicles that searched for parking
CirclingDistribution: JSON object representing the neighbouring geohashes at the 7 character level whereby vehicles searching for parking tend to spend their time. Each geohash will have the average percentage of time spent in that geohash prior to parking.
HourlyDistribution: JSON object representing the average prevalence of searching for parking by hour of day ( distribution based on number of vehicles experiencing parking problems)
SearchingByHour: JSON object representing the average percentage of vehicles searching for parking within the hour
PercentCar: Percentage of vehicles with parking issues that were cars
PercentMPV: Percentage of vehicles with parking issues that were multi purpose vehicles
PercentLDT: Percentage of vehicles with parking issues that were light duty trucks
PercentMDT: Percentage of vehicles with parking issues that were medium duty trucks
PercentHDT: Percentage of vehicles with parking issues that were heavy duty trucks
PercentOther: Percentage of vehicles with parking issues that were unknown classification

Content
This dataset is aggregated over the previous 6 months and is updated monthly. This data is publicly available from Geotab (geotab.com).
Inspiration
As some inspiration, here are some questions:

Which cities are the hardest to find parking?
By joining population data externally, can you determine a relationship between a region's population and the time that it takes to find parking?
Similarly, by finding external data, is there a correlation between GDP and parking times? What about average household income?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43564) of an [OpenML dataset](https://www.openml.org/d/43564). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43564/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43564/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43564/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

